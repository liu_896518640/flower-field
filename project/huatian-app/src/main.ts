import { createApp } from 'vue'
import { RouteMeta, createRouter, RouterOptions, RouteRecordRaw, createWebHashHistory } from 'vue-router'
import App from './App'
import 'amfe-flexible/index.js'
// 批量导入路由模块
const modules = import.meta.glob("./pages/**/*.tsx")
console.log('modules', modules)
async function run() {
    const routes: RouteRecordRaw[] = []
    // 遍历每一个module
    for (const module in modules) {
        // 取出module中的每一个组件
        const mod: any = await modules[module]()
        // 遍历module中导出的每一个组件（路由）
        for (const key in mod) {
            mod[key].displayName = key
            // home 路由
            if (key === 'Home') {
                routes.push({
                    path: '/',
                    name: 'home',
                    component: mod['Home']
                })
                continue
            }
            // 其他路由
            routes.push({
                path: '/' + key.toLowerCase(),
                name: key.toLowerCase(),
                component: mod[key]
            })
        }
    }

    const history = createWebHashHistory()
    const router = createRouter({
        routes,
        history
    })
    // console.log('routes', routes)
    const app = createApp(App, { routes })
    app.use(router).mount('#app')
}
run()

