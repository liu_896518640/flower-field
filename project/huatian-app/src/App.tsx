import { defineComponent, PropType, provide } from "vue";
import { RouteRecordRaw, RouterView } from "vue-router";
import classes from './app.module.scss'
import './main.css'
export default defineComponent({
    props: {
        routes: {
            type: Array as PropType<RouteRecordRaw[]>,
            required: true
        }
    },
    setup(props, ctx) {
        const token = sessionStorage['token']
        provide('token', token)
        console.log('router', props.routes)
        return () => {
            if(token){
                return <div>请登录...</div>
            }
            return <>
                <div class={classes.container}>
                    <RouterView />
                </div>
            </>
        }
    },
})