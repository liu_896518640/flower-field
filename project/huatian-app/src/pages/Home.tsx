import { Tabs, Icon, Flex } from '@huatian/components'
const { Tab } = Tabs
import { Discover,SocialDemo } from './components/Discover'
import { Message } from './components/Message'
import classes from './home.module.scss'
const MenuItem = ({ isActive, type, title }: {
    isActive: Boolean,
    title: String,
    type: string
}) => {
    function getIcon(type: string) {
        // console.log('type', type)
        switch (type) {
            case 'Home':
                return <Icon.default.Home />;
            case 'Discover':
                return <Icon.default.Discover />;
            case 'Message':
                return <Icon.default.Message />;
            default:
                return <Icon.default.Home />
        }
    }
    return <>
        <span style={{ color: isActive ? '#5cd347' : 'grey' }}>
            <Flex type='column' align='center'>
                {getIcon(type)}
                {title}
            </Flex>
        </span>
    </>
}
export const Home = () => {
    return <>
        <Tabs scrollBehavior='body'>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='Discover' title='发现' />
                }}
            >
                <p>发现...</p>
                <Discover />
            </Tab>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='Home' title='社交' />
                }}
            >
                <p>社交...</p>
                <SocialDemo/>
            </Tab>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='Message' title='消息' />
                }}
            >
                <p>消息...</p>
                <Message />
            </Tab>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='Message' title='我的' />
                }}
            >
                <p>我的...</p>
                
            </Tab>
        </Tabs>
    </>
}