import { defineComponent, reactive, ref } from "vue";
import { CardStack, SocialCardProps,DiscoverCard,DiscoverCardProps,ListView } from "@huatian/components";
import { wait } from '@huatian/utils'
import { Random } from 'mockjs'
async function mockData() {
    await wait(1000 + Math.random() * 1000)
    return [...Array(10)].map((_, i) => {
        return {
            id: i,
            img: Random.image('300x300')
        }
    })
}
async function mockDataDiscoverList() {
    await wait(1000 + Math.random() * 1000)
    return [...Array(10)].map((_, i) => {
        return {
            title: Random.ctitle(),
            content: Random.csentence(),
            cover: Random.image('200x100'),
            avatar: ['https://api.multiavatar.com/Binx Bond.svg', 'https://api.multiavatar.com/www.miigua.com.svg'][Math.floor(Math.random() * 2)]
        }
    })
}
function useCandidates() {
    const data = ref<SocialCardProps[]>([])
    const ver = ref(0)
    mockData().then(list => {
        data.value = data.value.concat(list)
        ver.value++
    })
    return {
        list: data,
        removeById(id: number) {
            data.value = data.value.filter(x => x.id !== id)
            ver.value++
        },
        ver
    }
}
function useDiscoverList() {
    const data = ref<DiscoverCardProps[]>([])
    async function load() {
        const list = await mockDataDiscoverList()
        if (data.value === null) {
            data.value = []
        }
        data.value = data.value?.concat(list)
    }
    load()
    return { list: data, loadMore: load }
}
export const SocialDemo = defineComponent({
    setup() {
        const { list, removeById, ver } = useCandidates()
        return () => {
            return <>
                <CardStack key={ver.value} list={list.value} onConfirm={(card) => {
                    removeById(card.id)
                }} />
            </>
        }
    }
})

export const Discover = defineComponent({
    setup(props, ctx) {
        const { list, loadMore } = useDiscoverList()
        return () => {
            return <ListView onBottom={loadMore}>
                {list.value === null && <div>loading...</div>}
                {
                    list.value && list.value.map((x, i) => {
                        return <DiscoverCard key={i} {...x} />
                    })
                }
            </ListView>
        }
    },
})