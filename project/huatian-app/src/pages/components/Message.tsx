import { defineComponent } from "vue";
import { MessageCard,ListView } from '@huatian/components'
export const Message = defineComponent({
    setup(props) {
        return () => {
            const list = [{
                avatar: 'https://api.multiavatar.com/www.miigua.com.svg',
                name: '王大爷',
                short: '你好啊 沙雕',
                time: '1分钟前',
                unread: 0
            }, {
                avatar: 'https://api.multiavatar.com/Binx Bond.svg',
                name: '张大娘',
                short: '你好啊 沙雕',
                time: '1分钟前',
                unread: 99
            }, {
                avatar: 'https://api.multiavatar.com/tarcrasher.svg',
                name: '小李子',
                short: '你好啊 沙雕',
                time: '1分钟前',
                unread: 99
            }]
            return <ListView>
                {
                    list.map(user => {
                       return <MessageCard {...user}/>
                    })
                }
            </ListView>
        }
    }
})

// ()=>{
//     const list = [{
//         avatar: 'http',
//         name: 'sb',
//         short: '你好啊 沙雕',
//         time: '1分钟前',
//         unread: 99
//     }, {
//         avatar: 'http',
//         name: 'sb',
//         short: '你好啊 沙雕',
//         time: '1分钟前',
//         unread: 99
//     }, {
//         avatar: 'http',
//         name: 'sb',
//         short: '你好啊 沙雕',
//         time: '1分钟前',
//         unread: 99
//     }]
//     return <ListView >
//         {list.map((user) => {
//                 <MessageCard {...user}>666</MessageCard>
//             })}
//     </ListView>
// },