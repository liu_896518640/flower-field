import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import jsx from '@vitejs/plugin-vue-jsx'
import postcssImport from 'postcss-import'
import postcssPxToRem from 'postcss-pxtorem'
// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 4000
  },
  plugins: [vue(), jsx()],
  css: {
    postcss: {
      plugins: [
        postcssImport,
        postcssPxToRem({
          rootValue: 37.5 ,
          propList: ['*'],
          unitPrecision: 5
        })
      ]
    }
  }
})
