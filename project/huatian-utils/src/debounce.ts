// 防抖函数
type FN = (...args: any[]) => any
export function debounce<T extends FN>(fn: T, delay = 300) : (...args : Parameters<T>) => ReturnType<T> {
    let I: any, lastResult: any
    return (...args: any[]) => {
        if(I) clearInterval(I)
        I = setTimeout(() => {
           lastResult = fn(...args)
        }, delay)
        return lastResult
    }
}

// test
// const fn = debounce((msg: number) => {
//     console.log(msg)
//     return msg
// })

// for(let i = 0; i < 100 ; i++){
//     fn(i)
// }