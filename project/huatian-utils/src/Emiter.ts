// Observer/Subscriber  观察者
// Observable/Publisher 发布者
// Topic / EventType    话题

// enum Topic {
//     Login = 'login',
//     Logout = 'logout'
// }
type EventHandler = (...args: any[]) => void
export class Emitter<EventType extends string | number> {

    private topics = new Map<EventType, EventHandler[]>()

    private getTopic(type: EventType) : EventHandler[] {
        if(!this.topics.has(type)) {
            this.topics.set(type, [])
        }
        return this.topics.get(type)
    }

    private removeHandler(type: EventType, handler: EventHandler) {
        if(!this.topics.has(type)){
            return
        }
        const handlers = this.topics.get(type).filter(x => x !== handler)
        this.topics.set(type, handlers)
    }

    on(type: EventType, handler: EventHandler) {
        const handlers = this.getTopic(type)
        handlers.push(handler)

        return () => {
            this.removeHandler(type, handler)
        }
    }

    emit(type: EventType, ...args: any[]) {
        const handlers = this.getTopic(type)

        handlers.forEach((handler, index) => {
            handler(...args)
        })
    }
}

enum Topics {
    Login
}
const a = new Emitter<Topics>()

const unsubscribe = a.on(Topics.Login, (msg) => {
    console.log(msg)
})

a.emit(Topics.Login, 'login')

// unsubscribe()

a.emit(Topics.Login, 'login Twice')