import { createApp } from 'vue'
import { RouteMeta, createRouter, RouterOptions, RouteRecordRaw, createWebHashHistory } from 'vue-router'
import App from './App'
// 批量导入路由
const modules = import.meta.glob("./stories/**/*.tsx")
console.log(modules)
async function run() {
    const routes: RouteRecordRaw[] = []
    // for in 可以遍历对象
    for (let module in modules) {
        const mod: any = await modules[module]()
        for (const key in mod) {
            // console.log(mod[key])
            routes.push({
                path: '/' + key.toLowerCase(),
                name: key.toLowerCase(),
                component: mod[key]
            })
        }

    }

    const history = createWebHashHistory()
    const router = createRouter({
        routes,
        history
    })
    // console.log('routes', routes)
    const app = createApp(App, { routes })
    app.use(router).mount('#app')
}
run()

