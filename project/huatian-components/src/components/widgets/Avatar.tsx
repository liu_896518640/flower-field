import { StyleValue } from 'vue'
import classes from './widgets.module.scss'

export const Avatar = ({ url, size = 'medium', style }: { url: string, size?: 'small' | 'medium' | 'large', style?: StyleValue }) => {
    return <img src={url} style={style} class={`${classes.avatar} ${classes[size]}`} />
}