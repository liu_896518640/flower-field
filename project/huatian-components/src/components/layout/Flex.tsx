import classes from './flex.module.scss'
import { defineComponent, Prop, PropType } from 'vue'
export const Flex = defineComponent({
    props: {
        type: {
            type: String as PropType<'row' | 'column'>,
            default: 'row'
        },
        className: {
            type: String
        },
        align: {
            type: String as PropType<'flex-start' | 'center' | 'flex-end'>
        },
        justify: {
            type: String as PropType<'space-around' | 'space-between' | 'center' | 'flex-start' | 'flex-end'>
        }
    },
    // setup 只会初始化一次
    setup(_, ctx) {
        // console.log('flex', _)
        // render Function 只要一来的变量发生了变化就会发生重绘
        return ({ type, className, align, justify }: {
            type: 'row' | 'column',
            className: String,
            align: 'flex-start' | 'center' | 'flex-end',
            justify: 'space-around' | 'space-between' | 'center' | 'flex-start' | 'flex-end'
        }) => {
            const finallyClass = `${classes.flex} ${classes[type]} ${className || ''} ${classes['align-' + align] || ''} ${classes['justify-' + justify] || ''}`
            // console.log('finally', finallyClass)
            // console.log('classes', classes)
            return <div class={finallyClass}>
                {ctx.slots.default!()}
            </div>
        }
    }
})