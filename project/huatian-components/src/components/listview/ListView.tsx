import { defineComponent, onMounted, PropType, onUnmounted, ref, watch } from "vue";
import classes from './listview.module.scss'

export const ListView = defineComponent({
    props: {
        onBottom: {
            type: Function as PropType<() => Promise<unknown>>
        }
    },
    setup({ onBottom }, ctx) {
        const bottomRef = ref<HTMLDivElement | null>(null)
        const loading = ref(false)
        ctx.expose({
            scrollToBottom: () => {
                window.scrollTo(0, Number.MAX_SAFE_INTEGER)
            }
        })
        watch(bottomRef, () => {
            console.log('onMounted')
            if (!bottomRef.value) {
                return
            }
            const options: IntersectionObserverInit = {
                root: null,
                threshold: [0.5, 0.7, 1],
                rootMargin: '100px'
            }
            const intersectionHandler: IntersectionObserverCallback = async (entries) => {
                for (const entry of entries) {
                    if (onBottom) {
                        loading.value = true
                        try {
                            await onBottom()
                        } finally {
                            loading.value = false
                        }
                    }
                }
            }
            const observe = new IntersectionObserver(intersectionHandler, options)
            observe.observe(bottomRef.value!)
        })
        return () => {
            return <div class={classes.listview}>
                {ctx.slots.default!()}
                <div ref={bottomRef} class={classes['bottom-bar']}></div>
            </div>
        }
    }
})