import { defineComponent, PropType } from "vue";
import { Avatar } from "../widgets/Avatar";
import classes from './card.module.scss'
console.log('aaaa', classes)
import { Flex } from "../layout/Flex";
export type ChatCardProps = {
    type: 'receive' | 'send',
    avatar: string,
    content: string
}
export const ChatCard = defineComponent({
    props: {
        type: {
            type: String as PropType<'receive' | 'send'>,
            required: true,
            default: 'receive'
        },
        avatar: {
            type: String,
            required: true
        },
        content: {
            type: String,
            required: true
        }
    },
    setup(props) {
        console.log('chart props', props)
        return () => {
            const avatar = <Avatar url={props.avatar} size={'small'} />
            const content = <Content content={props.content} />
            function renderMessage() {
                switch (props.type) {
                    case 'receive':
                        return [avatar, content]
                    case 'send':
                        return [content, avatar]
                }
            }
            console.log(renderMessage())
            return <div class={`${classes['chat-card']} ${classes[props.type]}`}>
                <Flex align="center" justify={props.type==='receive'?'flex-start':'flex-end'}>
                    {renderMessage()}
                </Flex>
            </div>
        }
    }
})

const Content = ({ content }: { content: String }) => {
    return <div class={classes['chat-content']}>{content}</div>
}