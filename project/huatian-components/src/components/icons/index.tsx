import Agree from './Agree'
import Chat from './Chat'
import Discover from './Discover'
import Home from './Home'
import Message from './Message'
export default {
    Agree,
    Chat,
    Discover,
    Home,
    Message
}