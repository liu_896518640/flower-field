import { defineComponent, withCtx } from "vue"

export const Page = defineComponent({
    setup(_, ctx) {
        return () => {
            return <div style={
                {
                    width: '375px',
                    height: '700px',
                    border: '1px solid #ddd',
                    boxSizing: 'content-box',
                    position: 'relative'
                }
            }>
                {ctx.slots.default!()}
            </div>
        }
    }
})