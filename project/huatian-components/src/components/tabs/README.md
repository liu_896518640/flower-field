实例用法
``` tsx

const MenuItem = ({isActive, type, title}) = {
    return <>
        <span style={{color: isActive? 'blue':'grey'}}>{title}</span>
        <Icon type={type} style={{color: isActive ? 'blue' : 'grey'}}/>
    </>
}
<Tabs>
    <Tab 
        renderMenu={
            ({isActive}) => <MenuItem={isActive} type='home' title='首页'>
        }
    >
        <div> 您好 </div>
    </Tab>
    <Tab
         renderMenu={
            ({isActive}) => <MenuItem={isActive} type='home' title='首页'>
        }
    >
        <table> 您好 </table>
    </Tab>
    <Tab
        renderMenu={
            ({isActive}) => <MenuItem={isActive} type='home' title='首页'>
        }
    >
        <div> 您好 </div>
    </Tab>
</Tabs>