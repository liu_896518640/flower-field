import { defineComponent, PropType, reactive, ref, Ref, provide, inject } from "vue";
import classes from './tab.module.scss'

// 定义 renderMenu 方法类型 用于初始化tab
type RenderMenuItem = ({ isActive }: { isActive: Boolean }) => JSX.Element

// 定义 TabMenu 类型
type TabMenu = {
    render: RenderMenuItem
}

const _Tabs = defineComponent({
    props: {
        defaultActiveIndex: {
            type: Number
        },
        scrollBehavior: {
            type: String as PropType<'body' | 'inner'>,
            default: 'body'
        }
    },
    setup(props, context) {
        console.log('Tabs Props', props)
        // debugger
        // 定义tabs 用来存储 Tab
        const tabs = reactive<TabMenu[]>([])

        // 定义一个索引字段 存储当前选中的索引呢
        const activeIndex = ref(props.defaultActiveIndex || 0)

        // 注入tabs 进行渲染
        provide('tabs', tabs)

        // 注入选中 索引
        provide('activeIndex', activeIndex)

        return () => {
            // 存放的是插槽的虚拟DOM 组成的一个数组
            const defaultSlot = context.slots.default!

            console.log('default', defaultSlot())

            // 为每一个虚拟DOM添加索引 和 选中索引
            const vNodes = defaultSlot().map((vNode, i) => {
                if (!vNode.props) {
                    vNode.props = {}
                }
                if (!vNode.props.style) {
                    vNode.props.style = {}
                }
                if(props.scrollBehavior==='body'){
                    vNode.props.style.overflow = 'unset'
                }else{
                    vNode.props.style['overflow'] = 'auto'
                }
                vNode.props.index = i
                vNode.props.activeIndex = activeIndex.value
                return vNode
            })

            console.log('vNode', vNodes)

            return (
                <div class={classes.tabs}>
                    <TabMenu
                        scrollBehavior={props.scrollBehavior}
                        onActiveIndexChanged={(idx) => {
                            activeIndex.value = idx
                        }} tabs={tabs} activeIndex={activeIndex.value} />
                    {vNodes}
                </div>
            )
        }
    }
})

const TabMenu = ({ tabs, activeIndex, onActiveIndexChanged, scrollBehavior }: {
    tabs: TabMenu[],
    activeIndex: Number,
    onActiveIndexChanged: (index: number) => void,
    scrollBehavior: 'body' | 'inner'
}) => {
    // debugger
    console.log('TabMenu', tabs, activeIndex)

    return (
        <div class={classes.menu} style={{ position: scrollBehavior === 'body' ? 'fixed' : 'absolute' }}>
            {
                tabs.map((tab, i) => {
                    console.log(tab)
                    return <div class={classes['menu-item']} onClick={() => onActiveIndexChanged && (activeIndex !== i) && onActiveIndexChanged(i)}>
                        {tab.render({ isActive: activeIndex === i })}
                    </div>
                })
            }
        </div>
    )
}

const Tab = defineComponent({
    props: {
        renderMenu: {
            required: true,
            type: Function as PropType<RenderMenuItem>
        },
        activeIndex: {
            type: Number
        },
        index: {
            type: Number
        },
        style: {
            type: Object
        }
    },
    setup(props, context) {
        // debugger
        console.log('Tab Props', props)

        // 获取注入的值 并对其进行类型断言
        const tabs = inject('tabs') as TabMenu[]
        tabs.push({
            render: props.renderMenu
        })

        return () => {
            // ！后置 表示是ts的特殊语法 表示当前变量不为空 或 未定义
            const defaultSlot = context.slots.default!
            const show = props.activeIndex === props.index
            return (
                <div class={classes.tab} style={{ display: show ? 'block' : 'none', ...props.style }}>
                    {defaultSlot()}
                </div>
            )
        }
    }
})


_Tabs.Tab = Tab

export const Tabs = _Tabs as typeof _Tabs & {
    Tab: typeof Tab
}