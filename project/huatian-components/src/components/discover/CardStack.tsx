import { computed, defineComponent, PropType, reactive, StyleValue, watch, ref } from "vue";
import classes from './cardstack.module.scss'
export type SocialCardProps = {
    img: string,
    id: number,
}
export const CardStack = defineComponent({
    props: {
        list: {
            type: Array as PropType<SocialCardProps[]>,
            required: true
        },
        onConfirm: {
            type: Function  as PropType<(card:SocialCardProps)=>void>
        }
    },
    setup({ list, onConfirm }, ctx) {
        console.log('render function stack')
        function confirm(card: SocialCardProps) {
            onConfirm && onConfirm(card)
        }
        // render 函数 而不是 虚拟dom
        return ({list}:{list : SocialCardProps[]}) => {
            return <div class={classes['card-stack']}>
                {
                    list.map((card, i) => {
                        return <Card key={card.id} card={card} index={i} onConfirm={() => confirm(card)} />
                    })
                }
            </div>
        }
    },
})

function useTouchEvents(enabled: boolean = true) {
    const diff = reactive({ x: 0, y: 0 })
    const state = ref<"init" | "start" | "release" | "move">("init")
    let startX: number = 0, startY: number = 0
    function handleStart(e: TouchEvent) {
        if (e.touches.length === 1) {
            startX = e.touches[0].clientX
            startY = e.touches[0].clientY
            state.value = 'start'
        }
    }
    function handleMove(e: TouchEvent) {
        if (e.touches.length === 1) {
            diff.x = e.touches[0].clientX - startX
            diff.y = e.touches[0].clientY - startY
            state.value = 'move'
        }
    }
    function handleEnd(e: TouchEvent) {
        // diff.x = 0
        // diff.y = 0
        state.value = 'release'
    }
    return {
        handlers: enabled ? {
            onTouchstart: handleStart,
            onTouchmove: handleMove,
            onTouchend: handleEnd
        } : {}, diff, state, reset: () => {
            diff.x = 0
            diff.y = 0
            state.value = 'init'
        }
    }
}
const Card = defineComponent({
    props: {
        card: {
            type: Object as PropType<SocialCardProps>,
            require: true
        },
        style: {
            type: Object as PropType<StyleValue>
        },
        index: {
            type: Number
        },
        onConfirm: {
            type: Function as PropType<(card: SocialCardProps) => void>,
        }
    },
    setup({ card, style, index, onConfirm }) {
        const { handlers, diff, state, reset } = useTouchEvents(index === 0)
        console.log('render Function', index )
        const trans = computed(() => {
            const W = window.innerWidth
            const r = diff.x / W
            return [r * 50, diff.x * 0.1]
        })
        watch(state, () => {
            if (state.value === 'release') {
                console.log('here---')
                if (trans.value[0] > 30) {
                    onConfirm && onConfirm(card)
                    console.log('confirm')
                } else {
                    console.log('cancel')
                    reset()
                }
            }
        })
        return () => {
            const style = {
                zIndex: 1000 - index,
                transformOrigin: 'bottom center',
                transform: `translate(${0 + trans.value[1]}px, ${index * 10}px) scale(${(1 - index / 50).toFixed(2)}) rotateZ(${trans.value[0]}deg)`
            }
            // console.log(diff.x, diff.y)
            return <div class={classes.card} style={style} {...handlers}>
                <img src={card.img} />
            </div>
        }
    }
})