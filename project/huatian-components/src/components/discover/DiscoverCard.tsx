import { defineComponent } from "vue";
import { Flex } from "../layout/Flex";
import { Avatar } from "../widgets/Avatar";
import Icon from '../icons'
import classes from './discover-card.module.scss'
export type DiscoverCardProps = {
    title: string,
    content: string,
    cover: string,
    avatar: string
}
export const DiscoverCard = defineComponent({
    props: {
        title: {
            type: String,
            required: true
        },
        content: {
            type: String,
            required: true
        },
        cover: {
            type: String,
            required: true
        },
        avatar: {
            type: String,
            required: true
        }
    },
    setup(props, ctx) {
        const { title, content, cover, avatar } = props
        return () => {
            return (
                <Flex type='column' class={classes['discover-card']}>
                    <Flex type='row' justify="space-between">
                        <Flex type='column'>
                            <h2 class={classes.title}>{title}</h2>
                            <p class={classes.content}>{content}</p>
                        </Flex>
                        <img class={classes.cover} src={cover} alt="" />
                    </Flex>
                    <Flex type='row' justify="space-between" align='center'>
                        <Avatar url={avatar} size='small'></Avatar>
                        <ActionPanel></ActionPanel>
                    </Flex> 
                </Flex>
            )
        }
    },
})

const ActionPanel = () => {
    return <div class={classes['action-panel']}>
        <Icon.Chat size="small" class={classes.icon}/>
        <Icon.Agree size="small" class={classes.icon}/>
    </div>
}