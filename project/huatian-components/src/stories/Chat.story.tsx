import { ListView } from "../components/listview/ListView";
import { ChatCard, ChatCardProps } from "../components/message/CahtCard";
import { defineComponent, ref } from "vue";
import { ChatBox } from "../components/message/ChatBox";
import { User, ChatSession } from '@huatian/domain'
const a1 = 'https://api.multiavatar.com/Binx Bond.svg'
const a2 = 'https://api.multiavatar.com/www.miigua.com.svg'
const list: ChatCardProps[] = [
    {
        avatar: a1,
        type: 'receive',
        content: '表哥 ！今天有时间吗😳'
    },
    {
        avatar: a2,
        type: 'send',
        content: '有啊 与啥事吗 老妹儿🐶'
    },
    {
        avatar: a1,
        type: 'receive',
        content: '出来玩啊 表哥 😁'
    },
    {
        avatar: a2,
        type: 'send',
        content: '哈哈哈哈 好啊'
    },
    {
        avatar: a1,
        type: 'receive',
        content: '咱们去跑山吧～'
    },
    {
        avatar: a2,
        type: 'send',
        content: '奥利给 要得😄'
    },
    {
        avatar: a1,
        type: 'receive',
        content: '哈哈哈哈 那我准备一下'
    },
    {
        avatar: a2,
        type: 'send',
        content: 'i will wait you'
    }
]

export const ChatCardDemo = () => {
    return <ListView>
        {
            list.map(item => {
                return <ChatCard {...item} />
            })
        }
    </ListView>
}
function useMockedChatSession() {
    const self = new User(1, '张三', a1)
    const to = new User(2, '李四', a2)
    const session = self.createChatSession(to)
    const list = ref(session.getChatList())

    session.on(ChatSession.Topics.ChatListChanged, () => {
        list.value = session.getChatList()
    })

    setTimeout(() => {
        session.receive(Math.random() + '')
    }, 3000)

    return { list, session }
}
export const ChatDemo = defineComponent({
    setup(props, ctx) {
        const { list, session } = useMockedChatSession()
        return () => {
            return <ChatBox list={list.value} onMessageEnter={message => {
                session.send(message)
            }} />
        }
    },
})