import { Tabs } from '../components/tabs'
import { Page } from '../components/Page'
const { Tab } = Tabs
const MenuItem = ({ isActive, type, title }: {
    isActive: Boolean,
    title: String,
    type: string
}) => {
    return <>
        <span style={{ color: isActive ? 'blue' : 'grey' }}>{title}</span>
    </>
}
export const demo = () => {
    return <div>
        <h2>Tabs 示例</h2>
        <Tabs scrollBehavior='body'>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='home' title='首页' />
                }}
            >
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
                <p>您好-------内容</p>
            </Tab>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='home' title='Hello' />
                }}
            >
                <p>hello--------内容</p>
            </Tab>
            <Tab
                renderMenu={({ isActive }) => {
                    return <MenuItem isActive={isActive} type='home' title='首页' />
                }}
            >
                <p>world--------内容</p>
            </Tab>
        </Tabs>
    </div >
}