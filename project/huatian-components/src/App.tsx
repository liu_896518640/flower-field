import { defineComponent, PropType, ref, Ref } from 'vue'
import { RouteRecordRaw, RouterLink, RouterView } from 'vue-router'
import classes from './app.module.scss'
import './main.css'
export default defineComponent({
    name: 'App',
    props: {
        routes: Array as PropType<RouteRecordRaw[]>
    },
    setup(props) {
        // console.log('props', props.routes)
        return () => {
            return <>
                <Menu routes={props.routes} />
                <div class={classes.container}>
                    <RouterView />
                </div>
            </>
        }
    }
})
function useToggle(): [Ref<Boolean>, () => void] {
    const value = ref(sessionStorage['toggle-value'] !== 'false')
    function toggle() {
        value.value = !value.value
        sessionStorage['toggle-value'] = value.value
    }
    return [value, toggle]
}
const Menu = defineComponent({
    props: {
        routes: {
            type: Array as PropType<RouteRecordRaw[]>,
            require: true
        }
    },
    setup(props) {
        const [toggleValue, toggle] = useToggle()
        window.addEventListener('keypress', (e) => {
            // console.log(e.ctrlKey, e.key)
            if (e.ctrlKey) {
                if (e.key === '/') {
                    toggle()
                }
            }
        })
        return () => {
            // console.log('prop', props.routes)
            return (
                <div class={classes.menu} style={{
                    display: toggleValue.value ? 'block' : 'none'
                }}>
                    <ul>
                        {
                            props.routes && props.routes.map(route => {
                                return <li key={route.name} onClick={() => toggle()}>
                                    <RouterLink to={route.path}>{route.name}</RouterLink>
                                </li>
                            })
                        }
                    </ul>
                    <button onClick={() =>
                        toggle()
                    } class={classes.toggle}>收起</button>
                </div>
            )

        }
    }
})