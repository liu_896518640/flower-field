
import { ChatSession } from './ChatSession'
import { Random } from 'mockjs'
// 导出user
export class User {
    // 构造user
    constructor(private id: number, private uname: string, private avatar: string) { }
    // 创建聊天
    createChatSession(to: User) {
        const session = new ChatSession(this, to)
        return session
    }
    // 获取用户头像
    getAvatar() {
        return this.avatar
    }
    // token
    public getId() {
        return this.id
    }
    public getName() {
        return this.uname
    }
}
// 导出userRepo 单例模式
export class UserRepo {
    private static repo: UserRepo
    public static getRepo() {
        if (!UserRepo.repo) {
            UserRepo.repo = new UserRepo()
        }
        return UserRepo.repo
    }
    // 内部变量users
    private users: Map<number, User>
    // 构造函数
    constructor() {
        this.users = new Map()
    }
    // 新增用户
    public add(user: User) {
        this.users.set(user.getId(), user)
    }
    // 根据id获取用户
    public getById(id: number) {
        return this.users.get(id)
    }
    // 获取所有用户
    public getAll() {
        return this.users.values()
    }
    // 随机生成用户
    static generate(imgList: string[], N: number) {
        const userList: User[] = []
        for (let i = 0; i < N; i++) {
            const user = new User(i, Random.cname(), imgList[Math.floor(Math.random() * imgList.length)])
            UserRepo.getRepo().add(user)
            userList.push(user)
        }
    }
}