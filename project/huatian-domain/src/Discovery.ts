
import { User } from './User'
export class DiscoveryRepo {
    
}

class DiscoveryItem {

    constructor(private title: string, private content: string, private cover: string, private user: User) { }

    public toJOSN() {
        return {
            title: this.title,
            content: this.content,
            cover: this.cover,
            avatar: this.user.getAvatar()
        }
    }
}
