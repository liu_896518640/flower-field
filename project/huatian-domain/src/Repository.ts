import { DiscoveryRepo } from './Discovery'
class Repository {
    public static readonly userRepo: UserRepo = new UserRepo()
    public static readonly discoveryRepo: DiscoveryRepo = new this.discoveryRepo()
}