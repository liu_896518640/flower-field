import { User } from './User';
import { Emitter } from '@huatian/utils';
type ChatRecord = {
    type: 'send' | 'receive'
    message: string
}
enum ChatSessionTopics {
    ChatListChanged
}
export class ChatSession extends Emitter<ChatSessionTopics>{

    private chatRecord: ChatRecord[] = []

    static Topics = ChatSessionTopics

    constructor(private from: User, private to: User) {
        super()
    }
    public getChatList() {
        console.log(this.chatRecord)
        return this.chatRecord.map(record => {
            return {
                type: record.type,
                content: record.message,
                avatar: record.type === 'send' ? this.from.getAvatar() : this.to.getAvatar()
            }
        })
    }
    public receive(msg: string) {
        this.chatRecord.push({
            type: 'receive',
            message: msg
        })
        this.emit(ChatSessionTopics.ChatListChanged)
    }

    public send(msg: string) {
        this.chatRecord.push({
            type: 'send',
            message: msg
        })
        this.emit(ChatSessionTopics.ChatListChanged)
    }
}
