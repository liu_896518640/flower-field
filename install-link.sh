#！ /bin/sh

cd $(dirname $0)
BASE=$(pwd)
echo $BASE
echo $(dirname $0)
cd $BASE/project/huatian-components
yarn link
cd $BASE/project/huatian-domain
yarn link
yarn link @huatian/utils
cd $BASE/project/huatian-rest
yarn link
cd $BASE/project/huatian-utils
yarn link
cd $BASE/project/huatian-app
yarn link @huatian/rest
yarn link @huatian/utils
yarn link @huatian/components
cd $BASE/project/huatian-components
yarn link @huatian/utils
yarn link @huatian/domain
cd $BASE/project/huatian-service
yarn link @huatian/domain