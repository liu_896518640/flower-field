#！ /bin/sh

cd $(dirname $0)
BASE=$(pwd)

projects=(huatian-app huatian-components huatian-domain huatian-rest huatian-service huatian-utils)
echo ${projects[@]}
for project in ${projects[@]}
do 
    echo "install $project"
    echo $BASE/project/$project
    cd $BASE/project/$project
done
